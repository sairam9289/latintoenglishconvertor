﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LatinToEnglishConvertor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void LatinLeft_Click(object sender, RoutedEventArgs e)
        {
           leftLabel.Content = "LEFT";
           leftLabel.Visibility = Visibility.Visible;
            rightLabel.Visibility = Visibility.Hidden;
            centerLabel.Visibility = Visibility.Hidden;
        }

        private void LatinRight_Click(object sender, RoutedEventArgs e)
        {
            rightLabel.Content = "RIGHT";
            rightLabel.Visibility = Visibility.Visible;
            leftLabel.Visibility = Visibility.Hidden;
            centerLabel.Visibility = Visibility.Hidden;
        }

        private void LatinCenter_Click(object sender, RoutedEventArgs e)
        {
            centerLabel.Content = "CENTER";
            centerLabel.Visibility = Visibility.Visible;
            leftLabel.Visibility = Visibility.Hidden;
            rightLabel.Visibility = Visibility.Hidden;

        }

        private void ResetTranslations_Click(object sender, RoutedEventArgs e)
        {
            leftLabel.Visibility =Visibility.Hidden;
            rightLabel.Visibility = Visibility.Hidden;
            centerLabel.Visibility = Visibility.Hidden;
        }
    }
}
