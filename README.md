# LatinToEnglishConvertor
LatinToEnglishConvertor is a Software Application designed for aiding with translation from Latin Language to English Language.
LatinToEnglishConvertor is developed using C#

## Installation And Project Setup
Download and install Visual Studio 2019
Download and Unzip the project(LatinToEnglishConvertor)

## Implementation
Open Visual Studio and Click On File -> Open -> Folder -> Select Folder(LatinToEnglishConvertor)
In Solution Explorer, Double Click MainWindow.xaml and MainWindow.xaml.cs
Click on Start to Run the Project Demo.

## License      
MIT License

Copyright (c) [2019] [Sai Ram Tammana]

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.